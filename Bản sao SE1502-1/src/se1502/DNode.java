/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se1502;

/**
 *
 * @author lephu
 */
public class DNode<T extends Comparable> {
    private T data;
    private DNode next, pre;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public DNode getNext() {
        return next;
    }

    public void setNext(DNode next) {
        this.next = next;
    }

    public DNode getPre() {
        return pre;
    }

    public void setPre(DNode pre) {
        this.pre = pre;
    }

    public DNode(T data) {
        this.data = data;
        pre = next = null;
    }
    
}
