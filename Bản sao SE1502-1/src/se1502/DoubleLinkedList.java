/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se1502;

/**
 *
 * @author lephu
 */
public class DoubleLinkedList<T extends Comparable>{
    private DNode<T> head, tail;

    public DoubleLinkedList() {
        head = tail = null;
    }
    
    public void AddToHead(T data)
    {
        DNode n = new DNode(data);
        if (IsEmpty()) head = tail = n;
        else
        {
            n.setNext(head);
            head.setPre(n);
            head = n;
        }
    }
    
    public void AddToTail(T data)
    {
        DNode n = new DNode(data);
        if (IsEmpty()) head = tail = n;
        else
        {
            tail.setNext(n);
            n.setPre(tail);
            tail = n;
        }
    }
    
    //add Dnode moi co du lieu la data vao sau p
    public void AddAfterNode(T data, DNode p)
    {
        if (IsEmpty() || p==null) {System.out.println("Khong add dc"); return;}
        if (p == tail) AddToTail(data);
        else
        {
            DNode n = new DNode(data);
            DNode q = p.getNext();
            //gan n vao sau p
            p.setNext(n); 
            n.setPre(p);
            //gan n vao truoc q
            q.setPre(n);
            n.setNext(q);
        }
    }
    
    //duyet ds
    public void Traverse()
    {
        if (IsEmpty()) System.out.println("Danh sach rong");
        else
        {
            DNode p = head;
            System.out.println("LinkedList:");
            while (p != null)
            {
                System.out.print(p.getData() + "\n");
                p = p.getNext();
            }
            System.out.println();
            System.out.println("Head: " + head.getData() + " Tail: " + tail.getData());
        }
    }
    
    //tim node chua data
    public DNode Search(T data)
    {
        if (IsEmpty()) return null;
        DNode p = head;
        while (p != null)
        {
            if (p.getData().equals(data)) return p;
            p = p.getNext();
        }        
        return null;
    }
    
    public boolean IsEmpty()
    {
        return (head == null);
    }
    
    public void Clear()
    {
        head = tail = null;
    }
    
    public void Sort()
    {
        if (IsEmpty()) return;
        DNode p = head;
        while (p!=null)
        {
            DNode q = p.getNext();
            while (q!=null)
            {
                if (p.getData().compareTo(q.getData()) > 0)
                {
                    Comparable temp = p.getData();
                    p.setData(q.getData());
                    q.setData(temp);
                }
                q = q.getNext();
            }
            p = p.getNext();
        }
    }
}
