/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se1502;

/**
 *
 * @author lephu
 */
public class SingleLinkedList<T> {
    private Node head, tail;

    public SingleLinkedList() {
        head = tail = null;
    }
    
    //add node data vao dau ds
    public void AddToHead(T data)
    {
        Node n = new Node(data);
        if (IsEmpty()) head = tail = n;
        else
        {
            n.setNext(head);
            head = n;
        }
    }
    
    //add node data vao cuoi ds
    public void AddToTail(T data)
    {
        Node n = new Node(data);
        if (IsEmpty()) head = tail = n;
        else
        {
            tail.setNext(n);
            tail = n;
        }
    }
    
    //add node data vao sau node p
    public void AddAfterNode(T data, Node p)
    {
        if (p == null) System.out.println("Khong add duoc");
        else
        {
            Node n = new Node(data);
            n.setNext(p.getNext());
            p.setNext(n);
            if (p == tail) tail = n;//truong hop sau khi add n tro thanh tail moi
        }
    }
    
    //add node data vao sau node co du lieu la previous
    public void AddAfter(T data, T previous)
    {
        Node p = Search(previous); //p la node co data = previous
        AddAfterNode(data, p);
    }
    
    //remove node dau ds
    public void RemoveHead()
    {
        if (IsEmpty()) System.out.println("Danh sach rong nen ko remove dc");
        else if (head == tail) head = tail = null;
        else head = head.getNext();
            
    }
    
    //remove node cuoi ds
    public void RemoveTail()
    {
        if (IsEmpty()) System.out.println("Ds rong, khong xoa dc");
        else if (head == tail) Clear();
        else //ds co it nhat 2 node
        {
            Node p = head;
            while (p.getNext() != tail) p = p.getNext();
            p.setNext(null);
            tail = p;
        }
    }
    
    //remove node co data
    public void RemoveNode(T data)
    {
        if (IsEmpty()) {System.out.println("Ds rong, ko xoa dc"); return;}
        if (head.getData().equals(data)) {RemoveHead(); return;}
        if (tail.getData().equals(data)) {RemoveTail(); return;}
        //nut can xoa ko phai head cung ko phai tail
        if (head == tail) {System.out.println("p ko co trong ds, ko xoa dc"); return;}
        Node q = head;
        while (true)
        {
            if (q.getNext().getData().equals(data))//tim dc q dung trc p can xoa
            {
                q.setNext(q.getNext().getNext());
                return;
            }
            if (q == tail) break;            
            q = q.getNext();
        }
        System.out.println("p khong co trong ds -> ko xoa dc");
    }
    
    public void RemoveNode(Node p)
    {
        if (p == null) {System.out.println("p null, ko xoa dc"); return;}
        if (IsEmpty()) {System.out.println("Ds rong, ko xoa dc"); return;}
        if (p == head) {RemoveHead(); return;}
        if (p == tail) {RemoveTail(); return;}
        //nut can xoa ko phai head cung ko phai tail
        if (head == tail) {System.out.println("p ko co trong ds, ko xoa dc"); return;}
        Node q = head;
        while (true)
        {
            if (q.getNext() == p)//tim dc q dung trc p can xoa
            {
                q.setNext(p.getNext());
                return;
            }
            if (q == tail) break;            
            q = q.getNext();
        }
        System.out.println("p khong co trong ds -> ko xoa dc");
    }
    
    //duyet ds
    public void Traverse()
    {
        if (IsEmpty()) System.out.println("Danh sach rong");
        else
        {
            Node p = head;
            System.out.println("LinkedList:");
            while (p != null)
            {
                System.out.print(p.getData() + " ");
                p = p.getNext();
            }
            System.out.println();
            System.out.println("Head: " + head.getData() + " Tail: " + tail.getData());
        }
    }
    
    //tim node chua data
    public Node Search(T data)
    {
        if (IsEmpty()) return null;
        Node p = head;
        while (p != null)
        {
            if (p.getData().equals(data)) return p;
            p = p.getNext();
        }        
        return null;
    }
    
    //kiem tra ds rong
    public boolean IsEmpty()
    {
        return (head == null);
    }
    
    //xoa sach ds
    public void Clear()
    {
        head = tail = null;
    }
}
