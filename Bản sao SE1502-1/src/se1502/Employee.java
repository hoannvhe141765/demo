/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se1502;

/**
 *
 * @author lephu
 */
public class Employee implements Comparable<Object>{
    private Integer code;
    private String name;

    public Employee(int code, String name) {
        this.code = code;
        this.name = name;
    }
    
    @Override
    public int compareTo(Object o) {
        Employee e = (Employee) o;
        return code.compareTo(e.code);
    }

    @Override
    public String toString() {
        return "Employee{" + "code=" + code + ", name=" + name + '}';
    }
    
    
    
}
